import 'package:flutter/material.dart';

const kPrimaryColor = Colors.teal;
const kPrimaryLightColor = Color(0xFFA7FFEB);//Colors.tealAccent[100];
const kPrimaryAccent = Colors.tealAccent;

