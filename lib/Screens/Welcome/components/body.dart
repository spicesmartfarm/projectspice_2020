import 'package:flutter/material.dart';
import 'package:spice_program/components/SignUpLoginCheck.dart';
import 'package:spice_program/components/rounded_button.dart';
import 'package:spice_program/components/rounded_input_field.dart';
import 'package:spice_program/components/rounded_password_field.dart';

import '../../../constants.dart';
import 'background.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/spice.png",
              width: size.width * 0.2,
            ),
            SizedBox(
              height: size.height * 0.01,
            ),
            Text(
              "Project SPICE",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            RoundedInputField(
              hintText: "Email",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "LOGIN",
              color: kPrimaryColor,
              textColor: Colors.white,
              onPress: () {},
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            SignUpLoginCheck(
              login: true,
              onPress: () {},
            ),
          ],
        ),
      ),
    );
  }
}
